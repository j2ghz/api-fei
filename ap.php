<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styly.css">
  <link rel="icon" type="image/png" href="pics/favicon.png">
  <title>FEI API od 2015</title>
  </head>
  
  <body>
  <table width="1200" align="center" border="0" cellpadding="10" cellspacing="0">
   <tr>
    <td width="200" valign="top">
    
    <div class="change" style="position: absolute;top: 20px;">
    <a style="position:fixed;" href="http://www.fei.stuba.sk/"><img class="change" src="pics/fei_logo.png" border="0" height="47" width="140"></a>
    </div>
    
    <div class="text_in_menu">
		<?php include 'menu.php';?>
    </div>
    </td>
    
    <td width="800" valign="top">
    
    <div class="text_in_heading">      
		<?php include 'heading.php';?>
    </div>
    
    <div class="text_in_body">
    
    <span class="mytable">
    <table width="0" cellspacing="0" cellpadding="0" border="1">
       <tr style="border-bottom:1px; border-bottom-color:rgb(240,185,56)">
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px" width="120">Dátum prednášky</td>
         <td style="padding-top:4px" width="40"><span class="change"><a href="files/ap/prezentacie/"><img src="pics/presentation1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">16.2.2016</td>
         <td><span class="change"><a href="files/ap/prezentacie/pap_16.2.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">23.2.2016</td>
         <td><span class="change"><a href="files/ap/prezentacie/pap_23.2.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">1.3.2016</td>
         <td><span class="change"><a href="files/ap/prezentacie/pap_1.3.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>                                                       
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">8.3.2016</td>
         <td><span class="change"><a href="files/ap/prezentacie/pap_8.3.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">15.3.2016</td>
         <td><span class="change"><a href="files/ap/prezentacie/pap_15.3.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">22.3.2016</td>
         <td><span class="change"><a href="files/ap/prezentacie/pap_22.3.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">29.3.2016</td>
         <td><img style="padding-top:4px" src="pics/holiday_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">5.4.2016</td>
         <td><span class="change"><a href="files/ap/prezentacie/pap_5.4.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">12.4.2016</td>
         <td><span class="change"><a href="files/ap/prezentacie/pap_12.4.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">19.4.2016</td>
         <td><span class="change"><a href="files/ap/prezentacie/pap_19.4.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">26.4.2016</td>
         <td><span class="change"><a href="files/ap/prezentacie/pap_26.4.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">3.5.2016</td>
         <td><span class="change"><a href="files/ap/prezentacie/pap_3.5.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">10.5.2016</td>
         <td><span class="change"><a href="files/ap/prezentacie/pap_10.5.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
    </table>
    </span><br> 
      
    <img src="arrow_o.png" border="0" height="12" width="12"> Materiály<br>
    <div style="padding-left:21px;width:500px">
    - <a href="files/ap/johnny_simulator.zip">Program Johnny (bez inštalácie)</a><br>
    - <a href="files/ap/johnny_guide.pdf">Príručka k Johnnymu</a><br>
    - <a href="files/ap/nahlad_skusky.pdf">Náhlad skúšky od prednášajúceho</a><br>
    - <a href="files/ap/ap_skripta_rok_2000.pdf">Skriptá (rok 2000)</a><br>
    - <a href="files/ap/moje_zadania_ap.zip">Zadania z ap</a><br>
    </div><br>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Skúška & zápočty<br>
    <div style="padding-left:21px">
    - <a href="files/ap/ap_skuska.zip">Skúšky z predošlých rokov</a><br>
    </div><br>
    
    </div>
    </td>
    
    <td width="300" valign="top">
    <div class="text_in_side"><br>
	
		<?php include 'news.php';?>
		
    </div>
    </td>
    
   </tr>
  </table>
  
  </body>
</html>   