<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styly.css">
  <link rel="icon" type="image/png" href="pics/favicon.png">
  <title>FEI API od 2015</title>
  </head>
  
  <body>
  <table width="1200" align="center" border="0" cellpadding="10" cellspacing="0">
   <tr>
    <td width="200" valign="top">
    
    <div class="change" style="position: absolute;top: 20px;">
    <a style="position:fixed;" href="http://www.fei.stuba.sk/"><img class="change" src="pics/fei_logo.png" border="0" height="47" width="140"></a>
    </div>
    
    <div class="text_in_menu">
		<?php include 'menu.php';?>
    </div>
    </td>
    
    <td width="800" valign="top">
    
    <div class="text_in_heading">      
		<?php include 'heading.php';?> 
    </div>
    
    <div class="text_in_body">
    
    <div>
    BASIC INFO
    <ul>
      <li>2 zápočtové písmky (jedna 20b)</li>
      <li>zápočet udelený pri aspoň súčte 20b</li>
      <li>skúška sa koná (60b)</li>
      <li>na zápočtoch (a možno aj na skúške) sú dovolené poznámky</li>
      <li>Web stránka predmetu ... <span class="change"><a target="_blank" href="http://www.uim.elf.stuba.sk/kaivt/Predmety/B-DUS"><img src="pics/web_icon.png" border="0" height="16" width="16"></a></span></li>      
    </ul>
    </div>
    
    <span class="mytable">
    <table width="0" cellspacing="0" cellpadding="0" border="1">
       <tr style="border-bottom:1px; border-bottom-color:rgb(240,185,56)">
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px" width="120">Číslo prednášky</td>
         <td style="padding-top:4px" width="40"><span class="change"><a href="files/dus/prednasky/"><img src="pics/presentation1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#1</td>
         <td><span class="change"><a href="files/dus/prednasky/pdus_1.pdf"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#2</td>
         <td><span class="change"><a href="files/dus/prednasky/pdus_2.pdf"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#3</td>
         <td><span class="change"><a href="files/dus/prednasky/pdus_3.pdf"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#4</td>
         <td><span class="change"><a href="files/dus/prednasky/pdus_4.pdf"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#5</td>
         <td><span class="change"><a href="files/dus/prednasky/pdus_5.pdf"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
    </table>
    </span><br>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Materiály<br>
    <div style="padding-left:21px">
    - <a href="files/dus/pn_editor.zip">PN Editor (aj s návodom na nainštalovanie)</a><br>
    </div><br>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Skúška & zápočty<br>
    <div style="padding-left:21px">
    - <a href="files/dus/zapocet_dus_1.zip">1. zápočtová písomka (zadania, nejaké riešenia)</a><br>
    </div><br>
    
    </div><br>
    
    </div>
    </td>
    
    <td width="300" valign="top">
    <div class="text_in_side"><br>
		
		<?php include 'news.php';?>
    
    </div>
    </td>
    
   </tr>
  </table>
  
  </body>
</html>   