<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styly.css">
  <link rel="icon" type="image/png" href="pics/favicon.png">
  <title>FEI API od 2015</title>
  </head>
  
  <body>
  <table width="1200" align="center" border="0" cellpadding="10" cellspacing="0">
   <tr>
    <td width="200" valign="top">
    
    <div class="change" style="position: absolute;top: 20px;">
    <a style="position:fixed;" href="http://www.fei.stuba.sk/"><img class="change" src="pics/fei_logo.png" border="0" height="47" width="140"></a>
    </div>
    
    <div class="text_in_menu">
		<?php include 'menu.php';?>
    </div>
    </td>
    
    <td width="800" valign="top">
    
    <div class="text_in_heading">      
		<?php include 'heading.php';?> 
    </div>
    
    <div class="text_in_body">
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Materiály<br>
    <div style="padding-left:21px">
    - <a href="files/f1/labaky_empty.zip">Prázne labáky na vytlačenie</a><br>
    - <a href="files/f1/labaky.zip">Vypracované labáky (z Marvina, netriedené)</a><br>
    - <a href="files/f1/si.pdf">Jednotky SI</a><br>
    - <a href="files/f1/vzorce_z_kinematiky.pdf">Vzorce z kinematiky</a><br>
    - <a href="files/f1/zbierka_1_vektorove_pocty.pdf">Zbierka úloh (vektorové počty)</a><br>
    - <a href="files/f1/zbierka_2_pohyb_latky.pdf">Zbierka úloh (pohyb látky)</a><br>
    </div><br>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Skúška & zápočty<br>
    <div style="padding-left:21px">
    - <a href="files/f1/vzorove_riesenia_od_Vajdu.zip">Nejaké vzorové riešenia ku zápočtom (Vajda)</a><br>
    - <a href="files/f1/zapoctove_priklady_vsetky_riesene.zip">Zápočtové príklady (všetky, riešené)</a><br>
    - <a href="files/f1/vypracovane_teor_otazky_na_skusku_tento_rok.zip">Vypracované teoretické otázky na skúšku (tento ročník)</a><br>
    </div><br>
    
    </div>
    </td>
    
    <td width="300" valign="top">
    <div class="text_in_side"><br>
	
		<?php include 'news.php';?>
    
    </div>
    </td>
    
   </tr>
  </table>
  
  </body>
</html>   