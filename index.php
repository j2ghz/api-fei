<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styly.css">
  <link rel="icon" type="image/png" href="pics/favicon.png">
  <title>FEI API od 2015</title>
  </head>
  
  <body>
  <table width="1200" align="center" border="0" cellpadding="10" cellspacing="0">
   <tr>
    <td width="200" valign="top">
    
    <div class="change" style="position: absolute;top: 20px;">
    <a style="position:fixed;" href="http://www.fei.stuba.sk/"><img class="change" src="pics/fei_logo.png" border="0" height="47" width="140"></a>
    </div>
    
    <div class="text_in_menu">
		<?php include 'menu.php';?>
    </div>
    </td>
    
    <td width="800" valign="top">
    
    <div class="text_in_heading">      
		<?php include 'heading.php';?>
    </div>
    
    <div class="text_in_body">
		Časom tu bude aj nejaký zmysluplný úvod ... vlastne asi nikdy ...<br><br>
		Na prehliadanie odporúčam <font color="blue">g</font><font color="red">o</font><font color="yellow">o</font><font color="blue">g</font><font color="green">l</font><font color="red">e</font> chrome.<br><br>
		
		<span class="mytable">
		  <table width="0" cellspacing="0" cellpadding="0" border="1">
		  <tr>
		  <td align="left" style="padding-left:3px;padding-top:3px;padding-right:3px;padding-bottom:3px;">
		  <u><span style="text-align:center;">Legenda:</span></u><br><br>
		  
		  <span class="change"><a href="files/troll.txt"><img style="position:relative;top:2px;" src="pics/download_icon1.png" border="0" height="14" width="14"></a></span> 
		  - priame sťahovanie<br>
		  
		  <span class="change"><a href="files/troll.txt"><img style="position:relative;top:2px;" src="pics/download_icon2.png" border="0" height="14" width="14"></a></span> 
		  - klikneš pravým a vyberieš "Uložiť odkaz ako..."<br>
		  
		  <span class="change"><a href="files/troll.txt"><img style="position:relative;top:2px;" src="pics/yellow_marker.png" border="0" height="14" width="14"></a></span> 
		  - prednáška/seminár sa konala, poznámky budú čoskoro<br>
		  
		  <span class="change"><a href="files/troll.txt"><img style="position:relative;top:2px;" src="pics/red_marker.png" border="0" height="14" width="14"></a></span> 
		  - prednáška alebo seminár nebola, nie je k dispozícií<br>
		  
		  <span class="change"><a href="files/troll.txt"><img style="position:relative;top:2px;" src="pics/holiday_marker.png" border="0" height="14" width="14"></a></span> 
		  - prednáška alebo seminár sa nekonala z dôvodu prázdnin (pravdepodobná náhrada)<br>
		  
		  <span class="change"><a href="files/troll.txt"><img style="position:relative;top:2px;" src="pics/zapocet_marker.png" border="0" height="14" width="14"></a></span> 
		  - prednáška alebo seminár sa nekonala z dôvodu zápočtu<br>
		  
		  <span class="change"><a href="files/troll.txt"><img style="position:relative;top:2px;" src="pics/ine_marker.png" border="0" height="14" width="14"></a></span> 
		  - prednáška alebo seminár sa nekonala z iných dôvodov (pr. štrajk)<br><br>
		  
		  Predmet označený ako <i>(o)</i> je opakovaný.
		  
		  </td>                                                                                                                      
		  </tr>
		  </table>
		  </span>
		
    </div>
    </td>
    
    <td width="300" valign="top">
    <div class="text_in_side"><br>
    
		<?php include 'news.php';?>
    
    </div>
    </td>
    
   </tr>
  </table>
	<style>#forkongithub a{background:#000;color:#fff;text-decoration:none;font-family:arial,sans-serif;text-align:center;font-weight:bold;padding:5px 40px;font-size:1rem;line-height:2rem;position:relative;transition:0.5s;}#forkongithub a:hover{background:#c11;color:#fff;}#forkongithub a::before,#forkongithub a::after{content:"";width:100%;display:block;position:absolute;top:1px;left:0;height:1px;background:#fff;}#forkongithub a::after{bottom:1px;top:auto;}@media screen and (min-width:800px){#forkongithub{position:fixed;display:block;top:0;left:0;width:200px;overflow:hidden;height:200px;z-index:9999;}#forkongithub a{width:200px;position:absolute;top:60px;left:-60px;transform:rotate(-45deg);-webkit-transform:rotate(-45deg);-ms-transform:rotate(-45deg);-moz-transform:rotate(-45deg);-o-transform:rotate(-45deg);box-shadow:4px 4px 10px rgba(0,0,0,0.8);}}</style><span id="forkongithub"><a href="https://gitlab.com/Arcanum417/api-fei">Fork me on GitLab</a></span>
  </body>
</html>   