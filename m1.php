<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styly.css">
  <link rel="icon" type="image/png" href="pics/favicon.png">
  <title>FEI API od 2015</title>
  </head>
  
  <body>
  <table width="1200" align="center" border="0" cellpadding="10" cellspacing="0">
   <tr>
    <td width="200" valign="top">
    
    <div class="change" style="position: absolute;top: 20px;">
    <a style="position:fixed;" href="http://www.fei.stuba.sk/"><img class="change" src="pics/fei_logo.png" border="0" height="47" width="140"></a>
    </div>
    
    <div class="text_in_menu">
		<?php include 'menu.php';?>
    </div>
    </td>
    
    <td width="800" valign="top">
    
    <div class="text_in_heading">      
		<?php include 'heading.php';?> 
    </div>
    
    <div class="text_in_body">
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Materiály<br>
    <div style="padding-left:21px">
    - <a href="files/m1/mat_analyza_satko.pdf">Matematická analýza (Satko)</a><br>
    - <a href="files/m1/riesene_priklady_z_nejakych_skript.pdf">Riesene priklady z nejakych skript</a><br>
    - <a href="files/m1/vzorce_pre_derivacie.zip">Derivačné vzorce</a><br>
    </div><br>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Skúška & zápočty<br>
    <div style="padding-left:21px">
    - <a href="files/m1/matematika_vypracovana_teoria.docx">Matematika vypracovaná teória</a><br>
    - <a href="files/m1/vzory_z_minulych_skusok_m1.zip">Skúšky z predošlých rokov</a><br>
    </div><br>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Príklady<br>
    <div style="padding-left:21px">
    - <a href="files/m1/priklady/priklady1.pdf">Príklady 1 (z ÚIM)</a><br>
    - <a href="files/m1/priklady/priklady2.pdf">Príklady 2 (z ÚIM)</a><br>
    - <a href="files/m1/priklady/priklady3.pdf">Príklady 3 (z ÚIM)</a><br>
    - <a href="files/m1/priklady/priklady4.pdf">Príklady 4 (z ÚIM)</a><br>
    - <a href="files/m1/priklady/priklady5.pdf">Príklady 5 (z ÚIM)</a><br>
    - <a href="files/m1/priklady/priklady6.pdf">Príklady 6 (z ÚIM)</a><br>
    - <a href="files/m1/priklady/priklady7.pdf">Príklady 7 (z ÚIM)</a><br>
    - <a href="files/m1/priklady/priklady8.pdf">Príklady 8 (z ÚIM)</a><br>
    - <a href="files/m1/priklady/priklady9.pdf">Príklady 9 (z ÚIM)</a><br>
    - <a href="files/m1/priklady/priklady10.pdf">Príklady 10 (z ÚIM)</a><br>
    - <a href="files/m1/priklady/priklady11.pdf">Príklady 11 (z ÚIM)</a><br>
    - <a href="files/m1/priklady/priklady12.pdf">Príklady 12 (z ÚIM)</a><br>
    </div><br> 
    
    </div>
    </td>
    
    <td width="300" valign="top">
    <div class="text_in_side"><br>
		<?php include 'news.php';?>
    </div>
    </td>
    
   </tr>
  </table>
  
  </body>
</html>   