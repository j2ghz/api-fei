<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styly.css">
  <link rel="icon" type="image/png" href="pics/favicon.png">
  <title>FEI API od 2015</title>
  </head>
  
  <body>
  <table width="1200" align="center" border="0" cellpadding="10" cellspacing="0">
   <tr>
    <td width="200" valign="top">
    
    <div class="change" style="position: absolute;top: 20px;">
    <a style="position:fixed;" href="http://www.fei.stuba.sk/"><img class="change" src="pics/fei_logo.png" border="0" height="47" width="140"></a>
    </div>
    
    <div class="text_in_menu">
		<?php include 'menu.php';?>
    </div>
    </td>
    
    <td width="800" valign="top">
    
    <div class="text_in_heading">      
		<?php include 'heading.php';?>
    </div>
    
    <div class="text_in_body">
    
    <div>
    BASIC INFO
    <ul>
      <li>3 zápočtové písomky, berú sa do úvahy 2 najlepšie</li>
      <li>zápočet udelený pri aspoň súčte 20b</li>
      <li>skúška sa koná (60b)</li>
      <li>kalkulačky na písomkách sú zakázané</li>
      <li>Web stránka predmetu ... <span class="change"><a target="_blank" href="http://matika.elf.stuba.sk/KMAT/Matematika2Opakovana/ParalelkaB"><img src="pics/web_icon.png" border="0" height="16" width="16"></a></span></li>      
    </ul>
    </div>
    
    <span class="mytable">
    <table width="0" cellspacing="0" cellpadding="0" border="1">
       <tr style="border-bottom:1px; border-bottom-color:rgb(240,185,56)">
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px" width="120">Dátum prednášky</td>
         <td style="padding-top:4px" width="40"><span class="change"><a href="files/m2o/prednasky/"><img src="pics/notes_logo.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">20.9.2016</td>
         <td><span class="change"><a href="files/m2o/prednasky/pm2o_20.9.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">27.9.2016</td>
         <td><span class="change"><a href="files/m2o/prednasky/pm2o_27.9.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">4.10.2016</td>
         <td><span class="change"><a href="files/m2o/prednasky/pm2o_4.10.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">11.10.2016</td>
         <td><span class="change"><a href="files/m2o/prednasky/pm2o_11.10.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">18.10.2016</td>
         <td><img style="padding-top:4px" src="pics/zapocet_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">25.10.2016</td>
         <td><img style="padding-top:4px" src="pics/yellow_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">1.11.2016</td>
         <td><img style="padding-top:4px" src="pics/holiday_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">8.11.2016</td>
         <td><img style="padding-top:4px" src="pics/yellow_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">15.11.2016</td>
         <td><img style="padding-top:4px" src="pics/yellow_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">22.11.2016</td>
         <td><img style="padding-top:4px" src="pics/yellow_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">29.11.2016</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">6.12.2016</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">??.12.2016</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
    </table>
    </span><br>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Materiály<br>
    <div style="padding-left:21px">
    - <a href="files/m2o/brillove_poznamky_a_priklady_2015.zip">Brillove príklady & poznámky (2015)</a><br>
    </div><br>
    
    </div>
    </td>
    
    <td width="300" valign="top">
    <div class="text_in_side"><br>
    
		<?php include 'news.php';?>
    
    </div>
    </td>
    
   </tr>
  </table>
  
  </body>
</html>   