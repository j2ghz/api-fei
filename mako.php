<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styly.css">
  <link rel="icon" type="image/png" href="pics/favicon.png">
  <title>FEI API od 2015</title>
  </head>
  
  <body>
  <table width="1200" align="center" border="0" cellpadding="10" cellspacing="0">
   <tr>
    <td width="200" valign="top">
    
    <div class="change" style="position: absolute;top: 20px;">
    <a style="position:fixed;" href="http://www.fei.stuba.sk/"><img class="change" src="pics/fei_logo.png" border="0" height="47" width="140"></a>
    </div>
    
    <div class="text_in_menu">
		<?php include 'menu.php';?>
    </div>
    </td>
    
    <td width="800" valign="top">
    
    <div class="text_in_heading">      
		<?php include 'heading.php';?>
    </div>
    
    <div class="text_in_body">
    
    <div>
    BASIC INFO
    <ul>
      <li>2 písomky (jedna 15b)</li>
      <li>skúška sa nekoná</li>
      <li>namiesto skúšky bude 3. písomka na poslednom cviku</li>
      <li>Web stránka predmetu (moodle) ... <span class="change"><a target="_blank" href="http://elearn.elf.stuba.sk/moodle/course/view.php?id=589"><img src="pics/web_icon.png" border="0" height="16" width="16"></a></span></li>      
    </ul>
    </div>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Materiály<br>
    <div style="padding-left:21px">
    - <a href="files/mako/mako_prednasky.zip">Všetky prednášky z MOODLE</a><br>
    </div><br>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Skúška & zápočty<br>
    <div style="padding-left:21px">
    - <a href="files/mako/zapocet_mako_1_2015.zip">1. zápočtová písomka (z 2015, moc to nemení)</a><br>
    - <a href="files/mako/zapocet_mako_2_2015.zip">2. zápočtová písomka (z 2015, moc to nemení)</a><br>
    </div><br>  
    
    </div>
    </td>
    
    <td width="300" valign="top">
    <div class="text_in_side"><br>
	
		<?php include 'news.php';?>
    
    </div>
    </td>
    
   </tr>
  </table>
  
  </body>
</html>   