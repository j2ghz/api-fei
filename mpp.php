<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styly.css">
  <link rel="icon" type="image/png" href="pics/favicon.png">
  <title>FEI API od 2015</title>
  </head>
  
  <body>
  <table width="1200" align="center" border="0" cellpadding="10" cellspacing="0">
   <tr>
    <td width="200" valign="top">
    
    <div class="change" style="position: absolute;top: 20px;">
    <a style="position:fixed;" href="http://www.fei.stuba.sk/"><img class="change" src="pics/fei_logo.png" border="0" height="47" width="140"></a>
    </div>
    
    <div class="text_in_menu">
		<?php include 'menu.php';?>
    </div>
    </td>
    
    <td width="800" valign="top">
    
    <div class="text_in_heading">      
		<?php include 'heading.php';?>
    </div>
    
    <div class="text_in_body">
    
    <div>
    BASIC INFO
    <ul>
      <li>2 zápočtové písmky (jedna 12b-13b)</li>
      <li>zápočet udelený pri aspoň celkovom súčte 20b (rátajú sa aj body za zadania)</li>
      <li>skúška sa koná (50b)</li>      
    </ul>
    </div>
    
    <span class="mytable">
    <table width="0" cellspacing="0" cellpadding="0" border="1">
       <tr style="border-bottom:1px; border-bottom-color:rgb(240,185,56)">
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px" width="120">Dátum prednášky</td>
         <td style="padding-top:4px" width="40"><span class="change"><a href="files/mpp/prednasky/"><img src="pics/presentation1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">21.9.2016</td>
         <td><span class="change"><a href="files/mpp/prednasky/pmpp_21.9.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">28.9.2016</td>
         <td><span class="change"><a href="files/mpp/prednasky/pmpp_28.9.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">5.10.2016</td>
         <td><span class="change"><a href="files/mpp/prednasky/pmpp_5.10.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">12.10.2016</td>
         <td><span class="change"><a href="files/mpp/prednasky/pmpp_12.10.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">19.10.2016</td>
         <td><span class="change"><a href="files/mpp/prednasky/pmpp_19.10.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">26.10.2016</td>
         <td><span class="change"><a href="files/mpp/prednasky/pmpp_26.10.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">2.11.2016</td>
         <td><span class="change"><a href="files/mpp/prednasky/pmpp_2.11.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">9.11.2016</td>
         <td><span class="change"><a href="files/mpp/prednasky/pmpp_9.11.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">16.11.2016</td>
         <td><img style="padding-top:4px" src="pics/yellow_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">23.11.2016</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">30.11.2016</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">7.12.2016</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
    </table>
    </span><br> 
    
    <span class="mytable_special">
    <table style="top: 104px" width="0" cellspacing="0" cellpadding="0" border="1">
       <tr style="border-bottom:1px; border-bottom-color:rgb(240,185,56)">
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px" width="120">Zadania</td>
         <td style="padding-top:4px" width="40"><span class="change"><a href="files/mpp/cvicenia/"><img src="pics/presentation1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#1</td>
         <td><span class="change"><a href="files/mpp/cvicenia/mpp_cviko_1.docx"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#2</td>
         <td><span class="change"><a href="files/mpp/cvicenia/mpp_cviko_2.docx"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#3</td>
         <td><span class="change"><a href="files/mpp/cvicenia/mpp_cviko_3.docx"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#4</td>
         <td><span class="change"><a href="files/mpp/cvicenia/mpp_cviko_4.docx"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#5</td>
         <td><span class="change"><a href="files/mpp/cvicenia/mpp_cviko_5.docx"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#6</td>
         <td><span class="change"><a href="files/mpp/cvicenia/mpp_cviko_6.docx"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#7</td>
         <td><span class="change"><a href="files/mpp/cvicenia/mpp_cviko_7.docx"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#8</td>
         <td><img style="padding-top:4px" src="pics/yellow_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#9</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#10</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#11</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#12</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
    </table>
    </span><br>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Skúška & zápočty<br>
    <div style="padding-left:21px">
    - <a href="files/mpp/zapocet_mpp_1.pdf">1. zápočtová písomka (vypracované komplet otázky)</a><br>
    </div><br>
    
    </div>
    </td>
    
    <td width="300" valign="top">
    <div class="text_in_side"><br>
	
		<?php include 'news.php';?>
    
    </div>
    </td>
    
   </tr>
  </table>
  
  </body>
</html>   