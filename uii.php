<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styly.css">
  <link rel="icon" type="image/png" href="pics/favicon.png">
  <title>FEI API od 2015</title>
  </head>
  
  <body>
  <table width="1200" align="center" border="0" cellpadding="10" cellspacing="0">
   <tr>
    <td width="200" valign="top">
    
    <div class="change" style="position: absolute;top: 20px;">
    <a style="position:fixed;" href="http://www.fei.stuba.sk/"><img class="change" src="pics/fei_logo.png" border="0" height="47" width="140"></a>
    </div>
    
    <div class="text_in_menu">
		<?php include 'menu.php';?>
    </div>
    </td>
    
    <td width="800" valign="top">
    
    <div class="text_in_heading">      
		<?php include 'heading.php';?>
    </div>
    
    <div class="text_in_body">
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Materiály<br>
    <div style="padding-left:21px">
    - <a href="files/uii/latex_guide.pdf">Návod na LaTeX</a><br>
    - <a href="files/uii/prednasky_z_moodle.zip">Prednášky z Moodle-u</a><br>
    - <a href="files/uii/zaklady_prace_v_matlabe.zip">Základy práce v Matlabe</a><br>
    - <a href="files/uii/zaverecne_zadanie.zip">Záverečné zadanie</a><br>
    </div><br>                                 
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Skúška & zápočty<br>
    <div style="padding-left:21px">
    - <a href="files/uii/vzory_z_minulych_skusok_uii.zip">Skúšky z predošlých rokov</a><br>
    </div><br> 
    
    </div>
    </td>
    
    <td width="300" valign="top">
    <div class="text_in_side"><br>
    
		<?php include 'news.php';?>
    
    </div>
    </td>
    
   </tr>
  </table>
  
  </body>
</html>   